/*************************************************************************************
Made by Roman Hyvönen.
*************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 1024

char *readFromConfig( const char * const filename, const char * const rowName ){
	char buffer[ BUFFER_SIZE ];
	char *target = NULL;
	char *value = NULL;
	FILE *file = fopen( filename, "r" );
	
	if( file == NULL ){
		printf( "Operation failed, no such file!\n" );
		return NULL;
	}
	
	while( fgets( buffer, BUFFER_SIZE, file ) != NULL ){
		target = strstr( buffer, rowName );
		if( target == buffer ){										//Target is same as buffer, if strstr found rowName and it begins from the start of line.
			if( target[ strlen( rowName )] == '=' ){				//If target is same as buffer, we can be sure it is long enough to have queryed element if config file is intact.
				value = malloc( strlen( buffer ) - strlen( rowName ) - 1 );
				
				if( value == NULL ){
					printf( "Operation failed!\n" );
					return NULL;
				}
				
				strcpy( value, target + strlen( rowName ) + 1 );	//Copying starts after "="-mark.
				break;
			}
		}
	}

	fclose( file );
	return value;	
}

int writeToConfig( const char * const filename, const char * const rowName, const char * const newValue ){
	char buffer[ BUFFER_SIZE ];
	char *target;
	int foundValue = 0;
	FILE *fileOld = fopen( filename, "r+" );
	
	if( fileOld == NULL ){
		printf( "Operation failed!\n" );
		return 0;
	}
	
	FILE *fileNew = fopen( "temp.txt", "w" );
	
	while( fgets( buffer, BUFFER_SIZE, fileOld ) != NULL ){
		target = strstr( buffer, rowName );
		if( target == buffer ){										//Target is same as buffer, if strstr found rowName and it begins from the start of line.
			if( target[ strlen( rowName )] == '=' ){				//If target is same as buffer, we can be sure it is long enough to have queryed element if config file is intact.
			
				fprintf( fileNew, "%s=%s\n", rowName, newValue );
				foundValue = 1;
				
			} else
				fputs( buffer, fileNew );
		} else{
			fputs( buffer, fileNew );
		}
	}
	
	if( foundValue == 0 ){
		printf( "Target attribute not found!\n" );
		fclose( fileOld );
		fclose( fileNew );
		remove( "temp.txt" );
		return 0;
	}
	
	fclose( fileOld );
	fclose( fileNew );
	remove( filename );
	rename( "temp.txt", filename );
	return 1;	
}



int main( int argc, char **argv ) {
	
	char *result = readFromConfig( "config.ini", "resolution" );
	printf( "Original value for resolution: %s\n", result );									
	writeToConfig( "config.ini", "resolution", "1920x1080" );			//1
	free( result );														//Better alternative would be static memory.
	result = readFromConfig( "config.ini", "resolution" );
	printf( "Modified value for resolution: %s\n", result );			
	free( result );
	writeToConfig( "config.ini", "nonExistantValue", "Nicolas Cage" );	//0
	writeToConfig( "config.ini", "windowmode", "windowed" );			//1
	
	return 0;
}
